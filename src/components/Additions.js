import React from 'react';
import '../styles/Additions.css';

export function PlayerX(props) {
    let turn = props.turn % 2 == 0 ? "activeInfo" : ""
    return <div className={"subInfo " + turn}>Player 1 (X)</div>
}

export function PlayerO(props) {
    let turn = props.turn  % 2 == 0 ? "" : "activeInfo"
    return <div className={"subInfo " + turn}>Player 2 (O)</div>
}

export function Counter(props) {
    return <div className="subInfo activeInfo"> {props.scoreCounter[0]}:{props.scoreCounter[1]}</div>
}

export function Timer(props) {
    return <div className="timer">{props.timer}</div>
}
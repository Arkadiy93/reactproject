import React from 'react';
import '../styles/Alert.css';

function Alert (props) {
    let hidden = props.alert ? " " : "hidden";
    return(
        <div className = {"alert " + hidden}>
        You have to finish your previous game
    <button className = "buttons alertButton">OK!</button>
    </div>
)
}

export default Alert;
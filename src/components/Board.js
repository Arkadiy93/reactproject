import React from 'react';
import '../styles/Board.css';
import Cell from './Cell';

class Board extends React.Component {
    render(){
        return(
            <div className = {"square"} onClick={(value) => this.props.setMark(parseInt(value.nativeEvent.target.className))}>{
            this.props.occupied.map((el,i) =>{
                return el.map((ele,j) => {
                    if(this.props.winnerPossition[0][0] == i && this.props.winnerPossition[0][1] == j || this.props.winnerPossition[1][0] == i && this.props.winnerPossition[1][1] == j || this.props.winnerPossition[2][0] == i && this.props.winnerPossition[2][1] == j ){
                return <Cell number={i*3+j} key = {i*3+j} occupied = {this.props.occupied[i][j]} winnerPossition = {true} ></Cell>
            }else {
                return <Cell number={i*3+j} key = {i*3+j} occupied = {this.props.occupied[i][j]} winnerPossition = {false}></Cell>
            }
        })
        })
        }
    </div>
    )}
}
//this.props.setMark(value.dispatchMarker.slice(-1))
export default Board
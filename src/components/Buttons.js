import React from 'react';
import '../styles/Buttons.css';

export function TimeAttackButton(props) {
    let blurred = props.alert ? "blurred" : "";
    return <button className={"buttons timeAttackButton " + blurred} data-name = {"TimeAttackButton"}> Time Attack / Multiplayer </button>
}

export function ResetButton(props) {
    return <button className = "buttons resetButton" data-name = {"ResetButton"}> reset </button>
}

export function BackButton(props) {
    return <button className="buttons backButton" data-name = {"BackButton"}> back </button>
}

export function SinglePlayerButton(props) {
    let blurred = props.alert ? "blurred " : "";
    return <button className = {"buttons singlePlayerButton " + blurred} data-name = {"SinglePlayerButton"}> Single player </button>
}

export function MultiPlayerButton(props) {
    let blurred = props.alert ? "blurred" : "";
    let lol = props.alert ? "disabled" : "";
    return <button className = {"buttons multiPlayerButton " + blurred} data-name = {"MultiPlayerButton"}> Multiplayer </button>
}

export function NewGameButton(props) {
    let game = props.game[0] ? "hidden" : "";
    return <button className={"buttons newGame " + game} data-name = {"NewGameButton"}> New game </button>;
}

import React from 'react';
import '../styles/Cell.css';

function Cell(props) {
    let onHover = props.occupied ? "  inSquareOccupied" : " inSquareAvailable";
    let winner = props.winnerPossition ? " winner" : "";
    return(
        <div className = {props.number + " inSquare " + onHover + winner }>
        {props.occupied}
</div>
);
}

export default Cell;
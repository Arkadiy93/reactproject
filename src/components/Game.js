import React from 'react';
import Alert from './Alert';
import Info from './Info';
import Board from './Board';
import * as constants from '../constants';
import {Timer} from './Additions';
import * as methods from './Methods';
import{
    TimeAttackButton,
    ResetButton,
    BackButton,
    SinglePlayerButton,
    MultiPlayerButton,
    NewGameButton
} from './Buttons';
import{Logo} from './Logo';
var R = require('ramda')


class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      occupied: [
        [null, null, null],
        [null, null, null],
        [null, null, null]
      ],
      player: true,

      // multiGame is a multiplayer mode game where two players play versus each other
      multiGame: [true, 0, null],// 0: if game is still playing, 1: how many turns have been played, 2: winner of the game

      // timeAttackGame is a multiplayer mode game where two players play versus each other and they have time limit 5 seconds in each round
      timeAttackGame: [false, 0, null],// 0: if game is still playing, 1: how many turns have been played, 2: winner of the game

      // singleGame is a singleplayer mode game where player plays versus computer
      singleGame: [true, 0, null],// 0: if game is still playing, 1: how many turns have been played, 2: winner of the game

      winnerPossition: [
        [null, null],
        [null, null],
        [null, null]
      ],
      scoreCounter: [0, 0],
      page: 0,
      timer: 5,
      timerId: null,
      flag: true,
      alert: false,
    };
    this.pickButton = this.pickButton.bind(this);
    this.setMark = this.setMark.bind(this);
    this.changeTimer = this.changeTimer.bind(this);
    this.startTimer = this.startTimer.bind(this);
    this.stopTimer = this.stopTimer.bind(this);
    //this.aiTurn = this.aiTurn.bind(this);
    this.singleplayerGame = this.singleplayerGame.bind(this);
    this.multiplayerGame = this.multiplayerGame.bind(this);
    this.timeAttackGame = this.timeAttackGame.bind(this);
    this.newGame = this.newGame.bind(this);
    this.reset = this.reset.bind(this);
    this.timeEnd = this.timeEnd.bind(this);

  }

  singleplayerGame(place) {
    let {state} = this;
    let xCoord = Math.floor(place / 3);
    let yCoord = place % 3;
    if (this.state.occupied[xCoord][yCoord] === null && this.state.singleGame[0] === true && typeof (methods.checkResult(constants.players[(this.state.singleGame [1] ) % 2], xCoord, yCoord, this.state.occupied)) == "object") {
      let pos = methods.checkResult(constants.players[(this.state.singleGame[1]) % 2], xCoord, yCoord, this.state.occupied);
      this.setState(R.pipe(
          R.assocPath(["singleGame", 1], this.state.singleGame[1] + 1),
          R.assocPath(["occupied", xCoord, yCoord], constants.players[(this.state.singleGame[1]) % 2]),
          R.assocPath(["winnerPossition"], [
            [Math.floor(pos[0] / 3), pos[0] % 3],
            [Math.floor(pos[1] / 3), pos[1] % 3],
            [Math.floor(pos[2] / 3), pos[2] % 3]]),
          R.assocPath(["singleGame", 0], false),
          R.assocPath(["scoreCounter", ((this.state.singleGame[1]) % 2)], this.state.scoreCounter[(this.state.singleGame[1]) % 2] + 1)
      )(state))
    } else if (this.state.occupied[xCoord][yCoord] == null && this.state.singleGame[0] == true) {
      this.setState(R.pipe(
          R.assocPath(["player"], !this.state.player),
          R.assocPath(["singleGame", 1], this.state.singleGame[1] + 1),
          R.assocPath(["occupied", xCoord, yCoord], constants.players[(this.state.singleGame[1]) % 2])
      )(state), () => {
        if (methods.checkDraw(this.state.occupied) == true) {
          this.setState({
            singleGame: [false, 0, null]
          })
        }
        //this.aiTurn(this.state.occupied,0,this.state.player,false)
      })
    }
  }

  multiplayerGame(place) {
    let {state} = this;
    let xCoord = Math.floor(place / 3);
    let yCoord = place % 3;
    if (this.state.occupied[xCoord][yCoord] == null && this.state.multiGame[0] == true && typeof(methods.checkResult(constants.players[(this.state.multiGame[1]) % 2], xCoord, yCoord, this.state.occupied)) == "object") {
      let pos = methods.checkResult(constants.players[(this.state.multiGame[1]) % 2], xCoord, yCoord, this.state.occupied);
      this.setState(R.pipe(
          R.assocPath(["multiGame", 1], this.state.multiGame[1] + 1),
          R.assocPath(["occupied", xCoord, yCoord], constants.players[(this.state.multiGame[1]) % 2]),
          R.assocPath(["winnerPossition"], [
            [Math.floor(pos[0] / 3), pos[0] % 3],
            [Math.floor(pos[1] / 3), pos[1] % 3],
            [Math.floor(pos[2] / 3), pos[2] % 3]]),
          R.assocPath(["multiGame", 0], false),
          R.assocPath(["scoreCounter", ((this.state.multiGame[1]) % 2)], this.state.scoreCounter[(this.state.multiGame[1]) % 2] + 1)
      )(state))
    } else if (this.state.occupied[xCoord][yCoord] == null && this.state.multiGame[0] == true) {
      this.setState(R.pipe(
          R.assocPath(["player"], !this.state.player),
          R.assocPath(["multiGame", 1], this.state.multiGame[1] + 1),
          R.assocPath(["occupied", xCoord, yCoord], constants.players[(this.state.multiGame[1]) % 2])
      )(state), () => {
        if (methods.checkDraw(this.state.occupied) == true) {
        this.setState({
          multiGame: [false, 0, null]
        })
      }
    })
    }
  }

  timeAttackGame(place) {

    let {state} = this;
    let xCoord = Math.floor(place / 3);
    let yCoord = place % 3;
    if (this.state.occupied[xCoord][yCoord] == null && this.state.timeAttackGame[0] == true && typeof(methods.checkResult(constants.players[(this.state.timeAttackGame[1]) % 2], xCoord, yCoord, this.state.occupied)) == "object" && this.state.timeAttackGame[0] != false) {

      this.stopTimer(this.state.timeAttackGame[1]);
      console.log("lol2");
      let pos = methods.checkResult(constants.players[(this.state.timeAttackGame[1]) % 2], xCoord, yCoord, this.state.occupied);
      this.setState(R.pipe(
          R.assocPath(["timer"], 5),
          R.assocPath(["timeAttackGame", 1], this.state.timeAttackGame[1] + 1),
          R.assocPath(["occupied", xCoord, yCoord], constants.players[(this.state.timeAttackGame[1]) % 2]),
          R.assocPath(["winnerPossition"], [
            [Math.floor(pos[0] / 3), pos[0] % 3],
            [Math.floor(pos[1] / 3), pos[1] % 3],
            [Math.floor(pos[2] / 3), pos[2] % 3]]),
          R.assocPath(["timeAttackGame", 0], false),
          R.assocPath(["scoreCounter", ((this.state.timeAttackGame[1]) % 2)], this.state.scoreCounter[(this.state.timeAttackGame[1]) % 2] + 1)
      )(state))
    } else if (this.state.occupied[xCoord][yCoord] == null && this.state.timeAttackGame[0] == true) {
      console.log("lol");
      this.stopTimer(this.state.timeAttackGame[1]);
      this.startTimer();
      this.setState(R.pipe(
          R.assocPath(["timer"], 5),
          R.assocPath(["player"], !this.state.player),
          R.assocPath(["timeAttackGame", 1], this.state.timeAttackGame[1] + 1),
          R.assocPath(["occupied", xCoord, yCoord], constants.players[(this.state.timeAttackGame[1]) % 2])
      )(state), () => {
        if (methods.checkDraw(this.state.occupied) == true) {
        this.setState({
          timeAttackGame: [false, 0, null]
        })
      }
    })

    }
  }

  timeEnd() {
    let {state} = this;
    console.log(this.state.timeAttackGame[1] % 2)
    clearInterval(this.state.timerId + this.state.timeAttackGame[1]);
    this.setState(R.pipe(
        R.assocPath(["occupied"], [
          [null, null, null],
          [null, null, null],
          [null, null, null]]),
        R.assocPath(["timeAttackGame"], [false, 0, null]),
        R.assocPath(["scoreCounter", (((this.state.timeAttackGame[1]) % 2) == 0 ? 1 : 0 )], this.state.scoreCounter[((this.state.timeAttackGame[1]) % 2) == 0 ? 1 : 0] + 1),
        R.assocPath(["timer"], [5])
    )(state))
  }

  startTimer() {
    let timerId = setInterval(() => this.changeTimer(), 1000);
    this.setState({timerId: timerId});
  }

  changeTimer() {
    let X = this.state.timer;
    X--;
    if (this.state.timer == 0) {
      this.timeEnd();
      this.stopTimer();
    } else {
      this.setState({timer: X})
    }
  }

  stopTimer(timer = 0) {
    clearInterval(this.state.timerId + timer);
    this.setState({timer: 5})
  }

  setMark(place) {
    if (this.state.page == 1) {
      this.singleplayerGame(place);
    } else if (this.state.page == 2) {
      this.multiplayerGame(place);
    } else if (this.state.page == 3) {
      this.timeAttackGame(place);
    }
  }


  pickButton(value){
    if (value.target.getAttribute('data-name') == "SinglePlayerButton"){
      this.setState({page:1})
    }else if (value.target.getAttribute('data-name') == "MultiPlayerButton"){
      this.setState({page:2})}
    else if(value.target.getAttribute('data-name') == "TimeAttackButton"){
      this.setState({page:3})
    }
    if (value.target.getAttribute('data-name') == "BackButton"){
      this.setState({page:0})
    }
    if (value.target.getAttribute('data-name') == "ResetButton"){
      this.reset();
    }
    if (value.target.getAttribute('data-name') == "NewGameButton"){
      this.newGame();
      if(this.state.page == 3){
        this.startTimer();
      }
    }
  }

  reset(){
    this.stopTimer(this.state.timeAttackGame[1]);
    this.setState({
      scoreCounter: [0,0],
      occupied : [
        [null, null, null],
        [null, null, null],
        [null, null, null]
      ],
      player : true,
      singleGame : [true, 0, null] ,
      multiGame : [true, 0, null] ,
      timeAttackGame : [false, 0, null] ,
      flag : true,
      winnerPossition :[
        [null, null],
        [null, null],
        [null, null]
      ]
    })
  }

  newGame(){
    this.setState({
      occupied : [
        [null, null, null],
        [null, null, null],
        [null, null, null]
      ],
      player : true,
      singleGame : [true, 0, null] ,
      multiGame : [true, 0, null] ,
      timeAttackGame : [true, 0, null] ,
      flag : true,
      winnerPossition :[
        [null, null],
        [null, null],
        [null, null]
      ]
    })
  }

  render() {
    if (this.state.page == 0) {
      return (
          <div onClick = {this.pickButton}>
    <Alert alert = {this.state.alert}/>
    <Logo occupied = {this.state.occupied} alert={this.state.alert}/>
    <SinglePlayerButton alert = {this.state.alert}/>
    <MultiPlayerButton alert = {this.state.alert}/>
    <TimeAttackButton alert = {this.state.alert}/>
    </div>
    )
    } else if (this.state.page == 1) {
      return (
          <div onClick={this.pickButton}>
    <BackButton/>
      <ResetButton/>
      <Board occupied = {this.state.occupied} setMark = {this.setMark} winnerPossition = {this.state.winnerPossition}/>
    <Info page = {this.state.page} game = {this.state.singleGame} scoreCounter = {this.state.scoreCounter}/>
    <NewGameButton game = {this.state.singleGame}/>
    </div>
    )
    } else if (this.state.page == 2) {
      return (
          <div onClick={this.pickButton}>
    <BackButton/>
      <ResetButton/>
      <Board occupied = {this.state.occupied} setMark = {this.setMark} winnerPossition = {this.state.winnerPossition}/>
    <Info page = {this.state.page} game = {this.state.multiGame} scoreCounter = {this.state.scoreCounter}/>
    <NewGameButton game = {this.state.multiGame}/>
    </div>
    )
    } else if (this.state.page == 3) {
      return (
          <div onClick={this.pickButton}>
    <BackButton/>
      <ResetButton/>
      <Timer timer = {this.state.timer}/>
    <Board occupied = {this.state.occupied} setMark = {this.setMark} winnerPossition = {this.state.winnerPossition}/>
    <Info page = {this.state.page}  game = {this.state.timeAttackGame} scoreCounter = {this.state.scoreCounter}/>
    <NewGameButton game = {this.state.timeAttackGame}/>
    </div>
    )
    }
  }
}

export default Game;

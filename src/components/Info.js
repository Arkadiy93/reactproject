import React from 'react';
import {
    PlayerX,
    PlayerO,
    Counter
} from './Additions';
import '../styles/Info.css';

function Info(props) {
    if (props.page == 1){
        return <div className = "info">
            <PlayerX turn = {props.game[1]}/>
    <Counter scoreCounter = {props.scoreCounter}/>
    <PlayerO turn = {props.game[1]}/>
    </div>
    }else if (props.page == 2){
        return <div className = "info">
            <PlayerX turn = {props.game[1]}/>
    <Counter scoreCounter = {props.scoreCounter}/>
    <PlayerO turn = {props.game[1]}/>
    </div>
    } else if (props.page == 3){
        return <div className = "info">
            <PlayerX turn = {props.game[1]}/>
    <Counter scoreCounter = {props.scoreCounter}/>
    <PlayerO turn = {props.game[1]}/>
    </div>
    }
}

export default Info;
import React from 'react';
import '../styles/Logo.css';
import {logoCells} from '../constants'

export function Logo (props) {
    let blurred = props.alert ? "blurred" : "";
    return(
        <div>
        <div className = {"logoSquare " +blurred}>{
        logoCells.map((el, i) => {
        return el.map((ele, j) => {
            return <LogoCell key={i * 3 + j} active={(i*3+j)==0 || (i*3+j)==3 || (i*3+j)==6 ? "winner": " " } player={ele}> </LogoCell>
})
})
}
</div>
    <span className={"logoText " + blurred}>Tic-Tac-Toe</span>
        </div>
)
}

export function LogoCell(props) {
    return <div className = {"logoCell " + props.active }> {props.player} </div>
}
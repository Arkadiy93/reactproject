import {winningCondition} from '../constants';

export function checkResult (player, xCoord, yCoord, occupied){
    let counter;
    let flag = false;
    winningCondition.forEach (( row ) => {
        counter = 0;
    row.forEach (( col ) => {
        if ( player == occupied [ Math.floor (col / 3)][col % 3]) {
        counter ++ ;
    }
    if(Math.floor (col / 3) == xCoord && col % 3 == yCoord) counter++;
    if ( counter == 3 ) {
        flag = row;
    }
})
});
    if(typeof (flag) ==  "object") return flag;
    return false;
}

export function checkDraw(occupied) {
    let flag = true;
    occupied.map((el, i) => {
        return el.map((ele, j) => {
            if (occupied[i][j] == null) {
        flag = false;
    }
})
});
    if (flag == true)
        return true;
    return false;
}
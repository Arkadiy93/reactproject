export const winningCondition =[
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
    [1, 4, 7]
];

export const logoCells = [
    ["X", "O", "O"],
    ["X", "X", " "],
    ["X", " ", "O"]
];

export const players = ["X", "O"];